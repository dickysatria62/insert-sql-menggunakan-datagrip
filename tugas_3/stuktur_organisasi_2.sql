create table employee
(
    id int not null,
    nama varchar(50) not null,
    atasan_id varchar(10) null,
    company_id int not null,
    foreign key (company_id) references company(id)
);

create table company
(
    id int not null,
    nama varchar(20) not null,
    alamat varchar(20) not null,
    PRIMARY KEY (id)
);

drop table employee;

insert into employee values ('1','Pak Budi', null,'1'),
                              ('2', 'Pak Tono','1','1'),
                              ('3', 'Pak Totok','1','1'),
                              ('4', 'Bu Sinta','2','1'),
                              ('5', 'Bu Novi','3','1'),
                              ('6', 'Andre','4','1'),
                              ('7', 'Dono','4','1'),
                              ('8', 'Ismir','5','1'),
                              ('9', 'Anto','5','1');

insert into company values ('1','PT JAVAN','SLeman'),
                            ('2','PT Dicoding','Bandung');

select * from company;
select * from employee;

#CEO adalah orang yang posisinya tertinggi. Buat query untuk mencari siapa CEO
select nama from employee Where atasan_id is null ;

#Staff biasa adalah orang yang tidak punya bawahan. Buat query untuk mencari siapa staff biasa
select nama from employee where atasan_id is not null and id not in (select distinct atasan_id from employee where atasan_id is not null );

#Direktur adalah orang yang dibawah langsung CEO. Buat query untuk mencari siapa direktur
select nama from employee where atasan_id = 1;

#Manager adalah orang yang dibawah direktur dan di atas staff.  Buat query untuk mencari siapa manager
select nama from employee where atasan_id in (select id from employee where atasan_id = 1);

#Buat sebuah query untuk mencari jumlah bawahan dengan parameter nama . Contoh kasus:
# Bawahan Bu Sinta berjumlah 2 orang
select count(id) from employee where atasan_id in (select id from employee where nama = 'Bu Sinta');

#Bawahan pak budi berjumlah 8 orang
select count(id) from employee where atasan_id in(select id from employee where nama = 'Pak Budi')
or atasan_id in (select id from employee where atasan_id in (select id from employee where nama = 'Pak Budi')
or atasan_id in (select id from employee where atasan_id in (select id from employee where nama = 'Pak Budi')));