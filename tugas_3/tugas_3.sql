CREATE TABLE karyawan
(
    id     int         not null ,
    nama   varchar(50) not null,
    jenis_kelamin varchar (1) not null,
    status varchar(10) not null,
    tanggal_lahir date not null,
    tanggal_masuk date not null,
    Departemen int not null,
    foreign key (Departemen) references departemen (id)

);
CREATE TABLE departemen (
    id int NOT NULL,
    nama varchar(50) NOT NULL,
    PRIMARY KEY (id)
);

insert into departemen values ('1','Manajemen'),
                              ('2', 'Pengembangan Bisnis'),
                              ('3', 'Teknisi'),
                              ('4', 'Analisis');

select * from departemen;

insert into karyawan values ('1','Rizki Saputra','L','Menikah', '1980-10-11', '2011-1-1', 1),
                            ('2', 'Farhan Reza','L','Belum', '1989-11-01', '2011-1-1', 1),
                            ('3', 'Riyando Adi', 'L', 'Menikah', '1977-1-25', '2011-1-1', 1),
                            (4 ,'Diego Manuel', 'L','Menikah','1983-2-22', '2012-9-4', 2),
                            (5, 'Satya Laksana', 'L', 'Menikah', '1981-1-12', '2011-3-19',2),
                            (6, 'Miguel Hernandez','L','Menikah','1994-10-16','2014-6-15',2),
                            (7, 'Putri Persada', 'P','Menikah','1988-1-30', '2013-4-14',2),
                            (8, 'Alma Safira', 'P', 'Menikah','1991-5-18', '2013-9-28', 3),
                            (9, 'Haqi Hafiz','L','Belum','1995-9-19', '2015-3-9', 3),
                            (10, 'Abi Isyawara', 'L', 'Belum', '1991-6-3', '2012-1-22',3),
                            (11,'Maman Kresna','L','Belum', '1993-8-21', '2012-9-15',3),
                            (12, 'Nadia Aulia', 'P', 'Belum', '1989-10-7', '2012-5-7', 4),
                            (13, 'Mutiara Rezki', 'P', 'Menikah', '1988-3-23','2013-5-21', 4),
                            (14, 'Dani Setiawan', 'L', 'Belum', '1986-2-11', '2014-11-30', 4),
                            (15, 'Budi Putra', 'L','Belum', '1995-10-23', '2015-12-3', 4);

select * from karyawan;

drop table karyawan