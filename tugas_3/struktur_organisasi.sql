CREATE TABLE organisasi
(
    id     int         not null ,
    nama   varchar(50) not null,
    atasan_id varchar(10) ,
    posisi varchar(50) not null
);

insert into organisasi values ('1','Rizki Saputra','null','Direktur'),
                              ('2', 'Farhan Reza','1','Manajer'),
                              ('3', 'Riyando Adi','1','Manajer'),
                              ('4', 'Andre','2','Staff pengembangan bisnis & teknisi'),
                              ('5', 'Dono','2','Staff pengembangan bisnis & teknisi'),
                              ('6', 'Ismir','3','Staff analis'),
                              ('7', 'Anto','3','Staff analis');

select * from organisasi;

select * from organisasi where atasan_id between '2' and '3';
